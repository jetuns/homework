package io.fourfinanceit.dao;


import io.fourfinanceit.domain.LoanExtension;
import org.springframework.data.repository.CrudRepository;

public interface LoanExtensionDao extends CrudRepository<LoanExtension, Long> {
}
