package io.fourfinanceit.dao;


import io.fourfinanceit.domain.LoanApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

public interface LoanApplicationDao extends JpaRepository<LoanApplication, Long> {
    long countByIpAndApplicationDateBetween(String ip, LocalDateTime minDateTime, LocalDateTime maxDateTime);
}
