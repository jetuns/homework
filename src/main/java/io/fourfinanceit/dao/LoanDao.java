package io.fourfinanceit.dao;


import io.fourfinanceit.domain.Loan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface LoanDao  extends CrudRepository<Loan, Long> {
    Collection<Loan> findByIp(String ip);
}
