package io.fourfinanceit.rounding;


import java.math.BigDecimal;

public final class RoundingHelper {
    private RoundingHelper() {}

    public static final int ROUNDING_MODE = BigDecimal.ROUND_HALF_UP;
    public static final int DECIMALS = 4;
    public static final int PRECISION = 23;


    public static BigDecimal roundedValue(Double value) {
        return value == null
                ? null
                : new BigDecimal(value).setScale(DECIMALS, ROUNDING_MODE);
    }

    public static BigDecimal roundedValue(Integer value) {
        return value == null
                ? null
                : new BigDecimal(value).setScale(DECIMALS, ROUNDING_MODE);
    }
}
