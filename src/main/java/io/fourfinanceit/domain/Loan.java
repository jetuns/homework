package io.fourfinanceit.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;

import static io.fourfinanceit.domain.LendingConstants.MAX_LENDING_AMOUNT;
import static io.fourfinanceit.domain.LendingConstants.MIN_LENDING_AMOUNT;
import static io.fourfinanceit.domain.LendingConstants.MIN_LENDING_TERM_DAYS;
import static io.fourfinanceit.rounding.RoundingHelper.*;

@Entity
public class Loan extends BaseEntity <Long> {

    public static final BigDecimal DEFAULT_INTEREST_RATE_DAILY = roundedValue(10);

    @Column(nullable = false)
    @NotNull
    @Min(value = MIN_LENDING_TERM_DAYS, message = "should be >= " + MIN_LENDING_TERM_DAYS)
    private Integer termDays;

    @Column(nullable = false, scale = DECIMALS, precision = PRECISION)
    @NotNull
    @Min(value = MIN_LENDING_AMOUNT, message = "should be >= " + MIN_LENDING_AMOUNT)
    @Max(value = MAX_LENDING_AMOUNT, message = "should be < " + MAX_LENDING_AMOUNT)
    private BigDecimal amount;

    @Column(nullable = false, scale = DECIMALS, precision = PRECISION)
    @NotNull
    private BigDecimal interestRate;

    @Column
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @NotNull
    private LocalDateTime issueDate;

    @Column
    @NotBlank(message = "should be not empty")
    @JsonIgnore
    private String ip;

    @OneToMany(mappedBy = "loan", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
    private List<LoanExtension> loanExtensions = new LinkedList<>();

    public Loan() {
    }

    public Integer getTermDays() {
        return termDays;
    }

    public void setTermDays(Integer termDays) {
        this.termDays = termDays;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public LocalDateTime getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(LocalDateTime issueDate) {
        this.issueDate = issueDate;
    }

    public List<LoanExtension> getLoanExtensions() {
        return loanExtensions;
    }

    public void setLoanExtensions(List<LoanExtension> loanExtensions) {
        this.loanExtensions = loanExtensions;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }


    @JsonSerialize(using = LocalDateTimeSerializer.class)
    public LocalDateTime getRepayDate() {
        LocalDateTime repayDate = issueDate.plus(termDays, ChronoUnit.DAYS);

        for (LoanExtension loanExtension : loanExtensions) {
            repayDate = repayDate.plus(loanExtension.getTermDays(), ChronoUnit.DAYS);
        }

        return repayDate;
    }

    public BigDecimal getRepayAmount() {
        BigDecimal interestAmount = calculateInterestAmount(amount, termDays, interestRate);

        loanExtensions.sort((LoanExtension ext1, LoanExtension ext2)
                -> ext1.getExtensionDate().compareTo(ext2.getExtensionDate()));

        BigDecimal extensionInterestRate = interestRate;

        for (LoanExtension loanExtension : loanExtensions) {
            extensionInterestRate = extensionInterestRate.add(
                    loanExtension.getRateFactor().multiply(extensionInterestRate));

            interestAmount = interestAmount.add(
                    calculateInterestAmount(amount, loanExtension.getTermDays(), extensionInterestRate));
        }

        return amount.add(interestAmount);
    }

    @JsonIgnore
    private static BigDecimal calculateInterestAmount(BigDecimal amount, int termDays, BigDecimal interestRate) {
        BigDecimal interestAmount = BigDecimal.ZERO;

        for (int i = 0; i < termDays; i++) {
            interestAmount = interestAmount.add(
                    amount.multiply(interestRate).divide(roundedValue(100), DECIMALS, ROUNDING_MODE));
        }

        return interestAmount;
    }
}
