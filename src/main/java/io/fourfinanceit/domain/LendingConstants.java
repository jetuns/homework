package io.fourfinanceit.domain;

public final class LendingConstants {
    private LendingConstants() {}

    public static final int MIN_LENDING_TERM_DAYS = 1;
    public static final int MIN_LENDING_AMOUNT = 1;
    public static final int MAX_LENDING_AMOUNT = 100;
}
