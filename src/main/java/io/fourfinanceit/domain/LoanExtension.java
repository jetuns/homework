package io.fourfinanceit.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import static io.fourfinanceit.domain.LendingConstants.MIN_LENDING_TERM_DAYS;
import static io.fourfinanceit.rounding.RoundingHelper.*;

@Entity
public class LoanExtension extends BaseEntity <Long> {

    public static final Integer DEFAULT_EXTENSION_TERM_DAYS = 7;

    public static final BigDecimal DEFAULT_EXTENSION_RATE_FACTOR_DAILY
            = roundedValue(1.5).divide(roundedValue(7), DECIMALS, ROUNDING_MODE);

    @Column(nullable = false)
    @Min(value = MIN_LENDING_TERM_DAYS, message = "should be >= " + MIN_LENDING_TERM_DAYS, groups = {Default.class, OnApply.class})
    @NotNull(groups = {Default.class, OnApply.class})
    private Integer termDays;

    @Column(nullable = false, scale = DECIMALS, precision = PRECISION)
    @NotNull
    private BigDecimal rateFactor;

    @ManyToOne
    @NotNull(message = "Loan must exist for LoanExtension")
    @JsonIgnore
    private Loan loan;

    @Column(nullable = false)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @NotNull
    private LocalDateTime extensionDate;

    public LoanExtension() {
    }

    public LoanExtension(Loan loan) {
        this.loan = loan;
    }

    public Integer getTermDays() {
        return termDays;
    }

    public void setTermDays(Integer termDays) {
        this.termDays = termDays;
    }

    public BigDecimal getRateFactor() {
        return rateFactor;
    }

    public void setRateFactor(BigDecimal rateFactor) {
        this.rateFactor = rateFactor;
    }

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }

    public LocalDateTime getExtensionDate() {
        return extensionDate;
    }

    public void setExtensionDate(LocalDateTime extensionDate) {
        this.extensionDate = extensionDate;
    }
}
