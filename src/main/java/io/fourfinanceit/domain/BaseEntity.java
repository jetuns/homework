package io.fourfinanceit.domain;

import javax.persistence.*;

@MappedSuperclass
public class BaseEntity<T>  {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private T id;


    public T getId() {
        return id;
    }

    public void setId(T id) {
        this.id = id;
    }
}
