package io.fourfinanceit.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import java.time.LocalDateTime;

import static io.fourfinanceit.domain.LendingConstants.MAX_LENDING_AMOUNT;
import static io.fourfinanceit.domain.LendingConstants.MIN_LENDING_AMOUNT;
import static io.fourfinanceit.domain.LendingConstants.MIN_LENDING_TERM_DAYS;


@Entity
public class LoanApplication extends BaseEntity <Long> {

    @Column(nullable = false)
    @NotNull
    @Min(value = MIN_LENDING_TERM_DAYS, message = "should be >= " + MIN_LENDING_TERM_DAYS, groups = {Default.class, OnApply.class})
    private Integer termDays;

    @Column(nullable = false)
    @NotNull
    @Min(value = MIN_LENDING_AMOUNT, message = "should be >= " + MIN_LENDING_AMOUNT, groups = {Default.class, OnApply.class})
    @Max(value = MAX_LENDING_AMOUNT, message = "should be < " + MAX_LENDING_AMOUNT, groups = {Default.class, OnApply.class})
    private Integer amount;

    @Column()
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @NotNull
    private LocalDateTime applicationDate;

    @Column
    @NotBlank
    private String ip;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    private Loan loan;

    public LoanApplication() {
    }

    public LoanApplication(Integer termDays, Integer amount, LocalDateTime applicationDate) {
        this.termDays = termDays;
        this.amount = amount;
        this.applicationDate = applicationDate;
    }

    public Integer getTermDays() {
        return termDays;
    }

    public void setTermDays(Integer termDays) {
        this.termDays = termDays;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public LocalDateTime getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(LocalDateTime applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }
}
