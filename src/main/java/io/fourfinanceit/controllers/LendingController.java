package io.fourfinanceit.controllers;

import io.fourfinanceit.domain.Loan;
import io.fourfinanceit.domain.LoanApplication;
import io.fourfinanceit.domain.LoanExtension;
import io.fourfinanceit.domain.OnApply;
import io.fourfinanceit.services.LendingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Digits;
import javax.validation.groups.Default;
import java.time.LocalDateTime;
import java.util.Collection;

import static java.time.LocalDateTime.now;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;


@RestController
@RequestMapping("/loans")
public class LendingController {


    @Autowired
    private LendingService lendingService;

    @Autowired
    public LendingController(LendingService lendingService) {
        this.lendingService = lendingService;
    }

    @GetMapping
    public Collection<Loan> handleUserLoans(HttpServletRequest request) {
        return lendingService.getUserLoans(request.getRemoteAddr());
    }

    @PutMapping
    public Loan createLoan(@Validated(OnApply.class) @RequestBody LoanApplication loanApplication, HttpServletRequest request) {
        loanApplication.setId(null);
        loanApplication.setIp(request.getRemoteAddr());
        loanApplication.setApplicationDate(now());

        return lendingService.issueLoan(loanApplication);

    }

    @PutMapping("/{loanId}/extensions")
    public Loan extendLoan(@PathVariable Long loanId, @Validated(OnApply.class) @RequestBody LoanExtension extensionRequest) {
        LoanExtension loanExtension = lendingService.extendLoan(loanId, extensionRequest.getTermDays());
        return loanExtension.getLoan();
    }
}
