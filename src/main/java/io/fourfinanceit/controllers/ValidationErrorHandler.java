package io.fourfinanceit.controllers;

import io.fourfinanceit.services.LoanNotFoundException;
import io.fourfinanceit.validation.CreditRiskException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice(assignableTypes = LendingController.class)
public class ValidationErrorHandler {

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    @ResponseBody
    public Collection<ValidationError> handleException(MethodArgumentNotValidException ex) {
        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();

        return fieldErrors.stream()
                .map(fieldError -> new ValidationError(fieldError.getDefaultMessage(), fieldError.getField()))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @ExceptionHandler(CreditRiskException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    @ResponseBody
    public Collection<ValidationError> handleRiskExcetion(CreditRiskException ex) {
        Collection<ValidationError> errors = new ArrayList<>();

        errors.add(new ValidationError(ex.getMessage(), ex.getClass().getSimpleName()));

        return errors;
    }


    @ExceptionHandler(LoanNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public Collection<ValidationError> handleLoanNotFoundException(LoanNotFoundException ex) {
        Collection<ValidationError> errors = new ArrayList<>();

        errors.add(new ValidationError(ex.getMessage(), ex.getClass().getSimpleName()));

        return errors;
    }
}
