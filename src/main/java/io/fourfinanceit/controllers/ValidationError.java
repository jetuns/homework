package io.fourfinanceit.controllers;


public class ValidationError {
    private final String validationErrorMessage;
    private final String cause;

    public ValidationError(String validationErrorMessage, String cause) {
        this.validationErrorMessage = validationErrorMessage;
        this.cause = cause;
    }

    public String getValidationErrorMessage() {
        return validationErrorMessage;
    }

    public String getCause() {
        return cause;
    }
}
