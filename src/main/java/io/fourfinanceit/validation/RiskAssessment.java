package io.fourfinanceit.validation;


import io.fourfinanceit.domain.LoanApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Set;

@Component
public class RiskAssessment {

    @Autowired
    private Collection<AssessmentRule<LoanApplication>> assessmentRules;

    public void assessRisk(LoanApplication loanApplication) {
        for (AssessmentRule<LoanApplication> rule : assessmentRules) {
            rule.fireRule(loanApplication);
        }
    }
}
