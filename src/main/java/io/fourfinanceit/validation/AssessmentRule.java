package io.fourfinanceit.validation;


public interface AssessmentRule<T> {
    void fireRule(T t);
}
