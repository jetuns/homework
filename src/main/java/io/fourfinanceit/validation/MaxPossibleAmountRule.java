package io.fourfinanceit.validation;


import io.fourfinanceit.domain.LoanApplication;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.temporal.ValueRange;

import static io.fourfinanceit.domain.LendingConstants.MAX_LENDING_AMOUNT;

@Component
public class MaxPossibleAmountRule implements AssessmentRule<LoanApplication> {
    private static final ValueRange NIGHT_HOURS = ValueRange.of(0, 8);

    public void fireRule(LoanApplication application) {
        if (application.getAmount().compareTo(MAX_LENDING_AMOUNT) >= 0 &&
                NIGHT_HOURS.isValidValue(application.getApplicationDate().getHour())) {

            throw new CreditRiskException("Unable to issue maximum possible amount nightly");
        }
    }
}
