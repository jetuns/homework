package io.fourfinanceit.validation;


import io.fourfinanceit.dao.LoanApplicationDao;
import io.fourfinanceit.domain.LoanApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Component
public class MaxApplicationsRule implements AssessmentRule<LoanApplication> {
    public static final long MAX_APPLICATIONS_PER_DAY = 3L;

    @Autowired
    private LoanApplicationDao loanApplicationDao;

    public void fireRule(LoanApplication application) {
        LocalDateTime now = LocalDateTime.now();

        long applicationsPerDay = loanApplicationDao.countByIpAndApplicationDateBetween(
                application.getIp(), now.with(LocalTime.MIN), now.with(LocalTime.MAX)
        );

        if (applicationsPerDay >= MAX_APPLICATIONS_PER_DAY) {
            throw new CreditRiskException("Daily maximum application allowance exceeded");
        }
    }
}
