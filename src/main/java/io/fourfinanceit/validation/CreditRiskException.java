package io.fourfinanceit.validation;


public class CreditRiskException extends RuntimeException {
    public CreditRiskException(String message) {
        super(message);
    }
}
