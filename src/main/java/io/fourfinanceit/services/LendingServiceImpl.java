package io.fourfinanceit.services;

import io.fourfinanceit.dao.LoanApplicationDao;
import io.fourfinanceit.dao.LoanDao;
import io.fourfinanceit.dao.LoanExtensionDao;
import io.fourfinanceit.domain.Loan;
import io.fourfinanceit.domain.LoanApplication;
import io.fourfinanceit.domain.LoanExtension;
import io.fourfinanceit.validation.RiskAssessment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;

import static io.fourfinanceit.domain.Loan.DEFAULT_INTEREST_RATE_DAILY;
import static io.fourfinanceit.domain.LoanExtension.DEFAULT_EXTENSION_RATE_FACTOR_DAILY;
import static io.fourfinanceit.domain.LoanExtension.DEFAULT_EXTENSION_TERM_DAYS;
import static io.fourfinanceit.rounding.RoundingHelper.roundedValue;


@Service
public class LendingServiceImpl implements LendingService {

    @Autowired
    private LoanApplicationDao loanApplicationDao;

    @Autowired
    private LoanDao loanDao;

    @Autowired
    private LoanExtensionDao loanExtensionDao;

    @Autowired
    private RiskAssessment riskAssessment;

    public Collection<Loan> getUserLoans(String ip) {
        if (ip == null || ip.isEmpty()) {
            throw new IllegalArgumentException("Please provide IP");
        }

        return loanDao.findByIp(ip);
    }

    @Override
    public Loan issueLoan(LoanApplication loanApplication) {
        riskAssessment.assessRisk(loanApplication);

        Loan loan = new Loan();
        loan.setAmount(roundedValue(loanApplication.getAmount()));
        loan.setTermDays(loanApplication.getTermDays());
        loan.setInterestRate(DEFAULT_INTEREST_RATE_DAILY);
        loan.setIssueDate(LocalDateTime.now());
        loan.setIp(loanApplication.getIp());

        loanApplication.setApplicationDate(LocalDateTime.now());
        loanApplication.setLoan(loan);
        loanApplicationDao.save(loanApplication);

        return loan;
    }

    @Override
    public LoanExtension extendLoan(Long loanId, Integer integer) {
        Loan loanToExtend = loanDao.findOne(loanId);

        if (loanToExtend == null) {
            throw new LoanNotFoundException("Loan not found id = " + String.valueOf(loanId));
        }

        LoanExtension extension = new LoanExtension(loanToExtend);
        extension.setTermDays(integer);
        extension.setRateFactor(DEFAULT_EXTENSION_RATE_FACTOR_DAILY);
        extension.setExtensionDate(LocalDateTime.now());

        loanToExtend.getLoanExtensions().add(extension);

        return loanExtensionDao.save(extension);
    }
}
