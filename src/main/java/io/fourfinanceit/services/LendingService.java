package io.fourfinanceit.services;


import io.fourfinanceit.domain.Loan;
import io.fourfinanceit.domain.LoanApplication;
import io.fourfinanceit.domain.LoanExtension;

import java.util.Collection;

public interface LendingService {

    Collection<Loan> getUserLoans(String ip);

    Loan issueLoan(LoanApplication loanApplication);

    LoanExtension extendLoan(Long loanId, Integer integer);
}
