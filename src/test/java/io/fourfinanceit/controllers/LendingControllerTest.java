package io.fourfinanceit.controllers;

import io.fourfinanceit.domain.Loan;
import io.fourfinanceit.domain.LoanExtension;
import io.fourfinanceit.services.LendingService;
import io.fourfinanceit.validation.CreditRiskException;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static io.fourfinanceit.domain.LocalDateTimeSerializer.DATE_TIME_FORMATTER;
import static io.fourfinanceit.rounding.RoundingHelper.roundedValue;
import static java.time.LocalDateTime.now;
import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(PowerMockRunner.class)
@PrepareForTest({LendingController.class})
public class LendingControllerTest {

    MockMvc mvc;

    @Mock
    LendingService lendingService;

    @Before
    public void setup() {
        mvc = standaloneSetup(new LendingController(lendingService))
                .setControllerAdvice(new ValidationErrorHandler())
                .build();
    }

    @Test
    public void should_NotAccept_When_ApplicationInvalid() throws Exception {
        mvc.perform(put("/loans")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"amount\":\"-1\",\"termDays\":\"1\"}"))
                .andExpect(status().isNotAcceptable()).andDo(print())
                .andExpect((jsonPath("$", hasSize(1))))
                .andExpect((jsonPath("$[0].validationErrorMessage", is("should be >= 1"))))
                .andExpect((jsonPath("$[0].cause", is("amount"))));
    }

    @Test
    public void should_HandleCreditRiskException_When_LoanIsRisky() throws Exception {
        given(lendingService.issueLoan(any())).willThrow(new CreditRiskException("loan is to risky"));

        mvc.perform(put("/loans")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"amount\":\"1\",\"termDays\":\"1\"}"))
                .andExpect(status().isNotAcceptable()).andDo(print())
                .andExpect((jsonPath("$", hasSize(1))))
                .andExpect((jsonPath("$[0].validationErrorMessage", is("loan is to risky"))));
    }

    @Test
    public void should_ReturnLoan_When_ApplicationIsValid() throws Exception {
        Loan loan = new Loan();
        loan.setAmount(roundedValue(100));
        loan.setInterestRate(roundedValue(10));
        loan.setTermDays(10);
        loan.setIssueDate(now());
        loan.setIp("127.0.0.1");

        given(lendingService.issueLoan(any())).willReturn(loan);

        mvc.perform(put("/loans")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"amount\":\"1\",\"termDays\":\"1\"}"))
                .andExpect(status().isOk()).andDo(print())
                .andExpect((jsonPath("$.amount", closeTo(100, 0.01))))
                .andExpect((jsonPath("$.termDays", is(10))))
                .andExpect((jsonPath("$.interestRate", closeTo(10, 0.01))))
                .andExpect((jsonPath("$.issueDate", is(now().format(DATE_TIME_FORMATTER)))))
                .andExpect((jsonPath("$.ip").doesNotExist()))
                .andExpect((jsonPath("$.loanExtensions", Matchers.hasSize(0))));
    }

    @Test
    public void should_ReturnLoanAndExtensions_When_LoanExtended() throws Exception {
        Loan loan = new Loan();
        loan.setAmount(roundedValue(100));
        loan.setInterestRate(roundedValue(10));
        loan.setTermDays(2);
        loan.setIssueDate(now());

        LoanExtension extensionByOneDay = new LoanExtension(loan);
        extensionByOneDay.setTermDays(1);
        extensionByOneDay.setRateFactor(LoanExtension.DEFAULT_EXTENSION_RATE_FACTOR_DAILY);
        extensionByOneDay.setExtensionDate(now());
        loan.getLoanExtensions().add(extensionByOneDay);

        given(lendingService.extendLoan(any(), any())).willReturn(extensionByOneDay);

        mvc.perform(put("/loans/1/extensions")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"termDays\":\"10\"}"))
                .andExpect(status().isOk()).andDo(print())
                .andExpect((jsonPath("$.loanExtensions", Matchers.hasSize(1))))
                .andExpect((jsonPath("$.loanExtensions[0].loan").doesNotExist()))
                .andExpect((jsonPath("$.loanExtensions[0].termDays", is(1))));
    }

    @Test
    public void should_ReturnUserLoansAndExtensions_When_RequestedByIp() throws Exception {
        Loan loan = new Loan();
        loan.setAmount(roundedValue(100));
        loan.setInterestRate(roundedValue(10));
        loan.setTermDays(2);
        loan.setIssueDate(now());

        LoanExtension extensionByOneDay = new LoanExtension(loan);
        extensionByOneDay.setTermDays(1);
        extensionByOneDay.setRateFactor(LoanExtension.DEFAULT_EXTENSION_RATE_FACTOR_DAILY);
        extensionByOneDay.setExtensionDate(now());
        loan.getLoanExtensions().add(extensionByOneDay);

        given(lendingService.getUserLoans(any())).willReturn(Collections.singletonList(loan));

        mvc.perform(get("/loans")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"term\":\"10\"}"))
                .andExpect(status().isOk()).andDo(print())
                .andExpect((jsonPath("$", hasSize(1))))
                .andExpect((jsonPath("$[0].loanExtensions", hasSize(1))));
    }
}
