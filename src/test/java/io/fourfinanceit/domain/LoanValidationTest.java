package io.fourfinanceit.domain;


import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.math.BigDecimal;
import java.util.Set;

import static java.time.LocalDateTime.now;
import static org.assertj.core.api.Assertions.assertThat;

public class LoanValidationTest {

    private static final String NULL_VALIDATION_MESSAGE = "may not be null";
    
    private Validator validator;

    @Before
    public void setup() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }


    @Test
    public void should_InvalidateLoan_With_EmptyTerm() {
        Set<ConstraintViolation<Loan>> violations = validator.validateProperty(new Loan(), "termDays");

        assertThat(violations).extracting("message").contains(NULL_VALIDATION_MESSAGE);
    }

    @Test
    public void should_InvalidateLoan_With_NegativeTerm() {
        Loan loan = new Loan();
        loan.setTermDays(-1);

        Set<ConstraintViolation<Loan>> violations = validator.validateProperty(loan, "termDays");

        assertThat(violations).extracting("message").contains("should be >= 1");
    }

    @Test
    public void should_InvalidateLoan_With_EmptyAmount() {
        Set<ConstraintViolation<Loan>> violations = validator.validateProperty(new Loan(), "amount");

        assertThat(violations).extracting("message").contains(NULL_VALIDATION_MESSAGE);
    }

    @Test
    public void should_InvalidateLoan_With_NegativeAmount() {
        Loan loan = new Loan();
        loan.setAmount(new BigDecimal(-1));

        Set<ConstraintViolation<Loan>> violations = validator.validateProperty(loan, "amount");

        assertThat(violations).extracting("message").contains("should be >= 1");
    }

    @Test
    public void should_InvalidateLoan_With_InterestRateNotSet() {
        Set<ConstraintViolation<Loan>> violations = validator.validateProperty(new Loan(), "interestRate");

        assertThat(violations).extracting("message").contains(NULL_VALIDATION_MESSAGE);
    }

    @Test
    public void should_InvalidateLoan_With_IssueDateNotSet() {
        Set<ConstraintViolation<Loan>> violations = validator.validateProperty(new Loan(), "issueDate");

        assertThat(violations).extracting("message").contains(NULL_VALIDATION_MESSAGE);
    }

    @Test
    public void should_InvalidateLoan_With_IpNotSet() {
        Set<ConstraintViolation<Loan>> violations = validator.validateProperty(new Loan(), "ip");

        assertThat(violations).extracting("message").contains("should be not empty");
    }

    @Test
    public void should_PassValidationForLoan_With_ValidAttributes() {
        Loan loan = new Loan();
        loan.setTermDays(10);
        loan.setAmount(BigDecimal.TEN);
        loan.setInterestRate(Loan.DEFAULT_INTEREST_RATE_DAILY);
        loan.setIp("127.0.0.1");
        loan.setIssueDate(now());

        Set<ConstraintViolation<Loan>> violations = validator.validate(loan);

        assertThat(violations).isEmpty();
    }
}
