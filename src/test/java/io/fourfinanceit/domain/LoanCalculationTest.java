package io.fourfinanceit.domain;


import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static io.fourfinanceit.domain.LoanExtension.DEFAULT_EXTENSION_RATE_FACTOR_DAILY;
import static io.fourfinanceit.rounding.RoundingHelper.roundedValue;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

public class LoanCalculationTest {

    @Test
    public void should_CalculateRepayDate_When_LoanHasNoExtensions() {
        LocalDateTime issueDate = LocalDateTime.of(2016, 1, 1, 0, 0);
        LocalDateTime expectedRepayDate = LocalDateTime.of(2016, 1, 2, 0, 0);

        Loan loan = new Loan();
        loan.setIssueDate(issueDate);
        loan.setTermDays(1);

        assertThat(loan.getRepayDate()).isEqualByComparingTo(expectedRepayDate);
    }

    @Test
    public void should_CalculateRepayDate_When_LoanHasExtensions() {
        LocalDateTime issueDate = LocalDateTime.of(2016, 1, 1, 0, 0);
        LocalDateTime expectedRepayDate = LocalDateTime.of(2016, 1, 5, 0, 0);

        Loan loan = new Loan();
        loan.setIssueDate(issueDate);
        loan.setTermDays(1);

        LoanExtension extensionByOneDay = new LoanExtension(loan);
        extensionByOneDay.setTermDays(1);

        LoanExtension extensionByTwoDays = new LoanExtension(loan);
        extensionByTwoDays.setTermDays(2);

        loan.getLoanExtensions().add(extensionByOneDay);
        loan.getLoanExtensions().add(extensionByTwoDays);

        assertThat(loan.getRepayDate()).isEqualByComparingTo(expectedRepayDate);
    }

    @Test
    public void should_CalculateRepayAmount_When_LoanHasNoExtensions() {
        Loan loan = new Loan();
        loan.setAmount(roundedValue(100));
        loan.setInterestRate(roundedValue(10));
        loan.setTermDays(3);

        assertThat(loan.getRepayAmount()).isCloseTo(roundedValue(130), within(new BigDecimal(0.01)));
    }

    @Test
    public void should_CalculateRepayAmount_When_LoanHasOneExtension() {
        Loan loan = new Loan();
        loan.setAmount(roundedValue(100));
        loan.setInterestRate(roundedValue(10));
        loan.setTermDays(2);

        LoanExtension extension = new LoanExtension(loan);
        extension.setTermDays(1);
        extension.setRateFactor(DEFAULT_EXTENSION_RATE_FACTOR_DAILY);
        extension.setExtensionDate(LocalDateTime.of(2016, 1, 1, 0, 0));
        loan.getLoanExtensions().add(extension);


        assertThat(loan.getRepayAmount()).isCloseTo(roundedValue(132.14), within(new BigDecimal(0.01)));
    }

    @Test
    public void should_CalculateRepayAmount_When_LoanHasMultipleExtensions() {
        Loan loan = new Loan();
        loan.setAmount(roundedValue(100));
        loan.setInterestRate(roundedValue(10));
        loan.setTermDays(2);

        LoanExtension extension = new LoanExtension(loan);
        extension.setTermDays(1);
        extension.setRateFactor(DEFAULT_EXTENSION_RATE_FACTOR_DAILY);
        extension.setExtensionDate(LocalDateTime.of(2016, 1, 1, 0, 0));
        loan.getLoanExtensions().add(extension);


        LoanExtension secondExtension = new LoanExtension(loan);
        secondExtension.setTermDays(1);
        secondExtension.setRateFactor(DEFAULT_EXTENSION_RATE_FACTOR_DAILY);
        secondExtension.setExtensionDate(LocalDateTime.of(2016, 1, 2, 0, 0));
        loan.getLoanExtensions().add(secondExtension);

        assertThat(loan.getRepayAmount()).isCloseTo(roundedValue(146.88), within(new BigDecimal(0.01)));
    }
}
