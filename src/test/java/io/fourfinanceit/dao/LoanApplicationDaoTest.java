package io.fourfinanceit.dao;

import io.fourfinanceit.domain.LoanApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static java.time.LocalDateTime.now;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class LoanApplicationDaoTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private LoanApplicationDao loanApplicationDao;

    private LoanApplication loanApplication;


    @Before
    public void setup() {
        loanApplication = new LoanApplication();

        loanApplication.setApplicationDate(now());
        loanApplication.setTermDays(10);
        loanApplication.setAmount(10);
        loanApplication.setIp("192.169.0.1");
    }

    @Test
    public void countWithinDateRangeShouldReturnOne() throws Exception {
        this.entityManager.persist(loanApplication);

        long count = loanApplicationDao.countByIpAndApplicationDateBetween(
                "192.169.0.1", now().with(LocalTime.MIN), now().with(LocalTime.MAX));
        assertThat(count).isEqualTo(1);
    }

    @Test
    public void countOutsideDateRangeShouldReturnZero() throws Exception {
        String ip = "192.169.0.1";
        LocalDateTime tomorrow = now().plusDays(1);

        LoanApplication loanApplication = new LoanApplication();
        loanApplication.setApplicationDate(now());
        loanApplication.setTermDays(10);
        loanApplication.setAmount(10);
        loanApplication.setIp(ip);

        this.entityManager.persist(loanApplication);

        long count = loanApplicationDao.countByIpAndApplicationDateBetween(
                "192.169.0.1", tomorrow.with(LocalTime.MIN), tomorrow.with(LocalTime.MAX));
        assertThat(count).isZero();
    }

}
