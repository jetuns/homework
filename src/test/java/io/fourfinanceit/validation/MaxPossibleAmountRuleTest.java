package io.fourfinanceit.validation;

import io.fourfinanceit.domain.LoanApplication;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;


public class MaxPossibleAmountRuleTest {

    AssessmentRule<LoanApplication> maxPossibleAmountRule = new MaxPossibleAmountRule();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void should_ThrowException_When_MaxPossibleAmountAtNight() {
        LocalDateTime night = now().withHour(1);

        thrown.expect(CreditRiskException.class);

        maxPossibleAmountRule.fireRule(new LoanApplication(10, 100, night));
    }

    @Test
    public void should_Pass_When_BellowMaxAmountAtNight() {
        LocalDateTime night = now().withHour(1);
        maxPossibleAmountRule.fireRule(new LoanApplication(10, 10, night));
    }

    @Test
    public void should_Pass_When_MaxPossibleAmountDayTime() {
        LocalDateTime day = now().withHour(10);
        maxPossibleAmountRule.fireRule(new LoanApplication(10, 300, day));
    }
}
