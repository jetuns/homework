package io.fourfinanceit.validation;

import io.fourfinanceit.domain.LoanApplication;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.Collection;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;


public class RiskAssessmentTest {

    @Mock
    private MaxApplicationsRule maxApplicationsRule;

    @Mock
    private MaxPossibleAmountRule maxPossibleAmountRule;

    @Spy
    Collection<AssessmentRule<LoanApplication>> assessmentRules = new ArrayList<>();

    @InjectMocks
    RiskAssessment riskAssessment;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        assessmentRules.add(maxApplicationsRule);
        assessmentRules.add(maxPossibleAmountRule);
    }

    @Test
    public void should_FireAssessmentRules() {
        riskAssessment.assessRisk(new LoanApplication());

        for (AssessmentRule<LoanApplication> assessmentRule : assessmentRules) {
            verify(assessmentRule).fireRule(any(LoanApplication.class));
        }
    }
}
