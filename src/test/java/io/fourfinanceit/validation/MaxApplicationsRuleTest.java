package io.fourfinanceit.validation;

import io.fourfinanceit.dao.LoanApplicationDao;
import io.fourfinanceit.domain.LoanApplication;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;


public class MaxApplicationsRuleTest {

    @Mock
    LoanApplicationDao loanApplicationDao;

    @InjectMocks
    MaxApplicationsRule maxApplicationsRule;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void should_ThrowException_When_ApplicationsCountRichMaximum() {
        given(loanApplicationDao.countByIpAndApplicationDateBetween(any(), any(),any())).willReturn(MaxApplicationsRule.MAX_APPLICATIONS_PER_DAY);

        thrown.expect(CreditRiskException.class);

        maxApplicationsRule.fireRule(new LoanApplication());
    }

    @Test
    public void should_Proceed_When_ApplicationsCountBelowMaximum() {
        given(loanApplicationDao.countByIpAndApplicationDateBetween(any(), any(),any())).willReturn(0L);

        maxApplicationsRule.fireRule(new LoanApplication());
    }



}
