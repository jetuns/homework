package io.fourfinanceit.services;

import io.fourfinanceit.dao.LoanApplicationDao;
import io.fourfinanceit.dao.LoanDao;
import io.fourfinanceit.dao.LoanExtensionDao;
import io.fourfinanceit.domain.Loan;
import io.fourfinanceit.domain.LoanApplication;
import io.fourfinanceit.domain.LoanExtension;
import io.fourfinanceit.validation.CreditRiskException;
import io.fourfinanceit.validation.RiskAssessment;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

import static io.fourfinanceit.domain.LoanExtension.DEFAULT_EXTENSION_RATE_FACTOR_DAILY;
import static io.fourfinanceit.domain.LoanExtension.DEFAULT_EXTENSION_TERM_DAYS;
import static io.fourfinanceit.rounding.RoundingHelper.roundedValue;
import static java.time.LocalDateTime.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({LocalDateTime.class, LendingServiceImpl.class})
public class LendingServiceTest {

    @Mock
    private LoanDao loanDao;

    @Mock
    private LoanApplicationDao loanApplicationDao;

    @Mock
    private LoanExtensionDao loanExtensionDao;

    @Mock
    private RiskAssessment riskAssessment;

    @InjectMocks
    LendingServiceImpl lendingService;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();


    @Test
    public void should_CheckRisks_When_IssueLoan() throws Exception {
        lendingService.issueLoan(new LoanApplication());

        verify(riskAssessment).assessRisk(any(LoanApplication.class));
    }

    @Test
    public void should_ThrowException_When_IssueRiskyLoan() {
        doThrow(CreditRiskException.class).when(riskAssessment).assessRisk(any());

        thrown.expect(CreditRiskException.class);
        lendingService.issueLoan(new LoanApplication());
    }


    @Test
    public void should_ApplyDefaultInterestRate_When_IssueLoan() throws Exception {
        when(loanDao.save(any(Loan.class))).then(returnsFirstArg());

        Loan loan = lendingService.issueLoan(new LoanApplication());

        assertThat(loan.getInterestRate()).isEqualTo(Loan.DEFAULT_INTEREST_RATE_DAILY);
    }

    @Test
    public void should_SetApplicationTermAndAmount_When_IssueLoan() throws Exception {
        when(loanDao.save(any(Loan.class))).then(returnsFirstArg());

        LoanApplication loanApplication = new LoanApplication(10, 10, now());

        Loan loan = lendingService.issueLoan(loanApplication);

        assertThat(loan.getTermDays()).isEqualTo(loanApplication.getTermDays());
        assertThat(loan.getAmount()).isEqualTo(roundedValue(loanApplication.getAmount()));
    }

    @Test
    public void should_SetApplicationIp_When_IssueLoan() throws Exception {
        when(loanDao.save(any(Loan.class))).then(returnsFirstArg());

        LoanApplication loanApplication = new LoanApplication(10, 10, now());
        loanApplication.setIp("127.0.0.1");

        Loan loan = lendingService.issueLoan(loanApplication);

        assertThat(loan.getIp()).isEqualTo(loanApplication.getIp());
    }

    @Test
    public void should_SetIssueDate_When_IssueLoan() throws Exception {
        LocalDateTime now = now();

        PowerMockito.mockStatic(LocalDateTime.class);
        given(now()).willReturn(now);

        when(loanDao.save(any(Loan.class))).then(returnsFirstArg());

        Loan loan = lendingService.issueLoan(new LoanApplication());

        assertThat(loan.getIssueDate()).isEqualTo(now);
    }

    @Test
    public void should_ThrowException_When_ExtendNonExistingLoan() {
        given(loanDao.findOne(any())).willReturn(null);

        thrown.expect(LoanNotFoundException.class);

        lendingService.extendLoan(-1L, 1);
    }

    @Test
    public void should_SetExtensionDate_When_ExtendLoan() throws Exception {
        LocalDateTime now = now();

        PowerMockito.mockStatic(LocalDateTime.class);
        given(now()).willReturn(now);
        given(loanDao.findOne(any())).willReturn(new Loan());

        when(loanExtensionDao.save(any(LoanExtension.class))).then(returnsFirstArg());

        LoanExtension loanExtension = lendingService.extendLoan(-1L, 0);

        assertThat(loanExtension.getExtensionDate()).isEqualTo(now);
    }

    @Test
    public void should_SetTermAndRateFactor_When_ExtendLoan() throws Exception {
        given(loanDao.findOne(any())).willReturn(new Loan());
        when(loanExtensionDao.save(any(LoanExtension.class))).then(returnsFirstArg());


        LoanExtension loanExtension = lendingService.extendLoan(-1L, 2);
        assertThat(loanExtension.getTermDays()).isEqualTo(2);
        assertThat(loanExtension.getRateFactor()).isEqualTo(DEFAULT_EXTENSION_RATE_FACTOR_DAILY);
    }

    @Test
    public void should_ReturnUserLoans_When_FindByClientIp() throws Exception {
        Collection<Loan> expectedUserLoans = new ArrayList<>();
        expectedUserLoans.add(new Loan());

        given(loanDao.findByIp(any())).willReturn(expectedUserLoans);

        Collection<Loan> actualUserLoans = lendingService.getUserLoans("127.0.0.1");

        assertThat(actualUserLoans).isNotEmpty();
        assertThat(actualUserLoans).size().isEqualTo(1);
    }

    @Test
    public void should_ThrowException_When_InvalidClientIp() throws Exception {
        thrown.expect(IllegalArgumentException.class);

        lendingService.getUserLoans(null);
    }
}
